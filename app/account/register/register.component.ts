import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ElementRef
} from "@angular/core";
import { IRegisterForm, IUrlForm } from "~/passit-frontend/account/register/interfaces";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { RegisterStages } from "~/passit-frontend/account/constants";
import { ActionsSubject } from "@ngrx/store";
import { AppTextFieldComponent } from "../../shared/text-field/text-field.component";
import { FormattedString, Span } from "tns-core-modules/text/formatted-string";
import { SimpleChanges } from "@angular/core";

@Component({
  selector: "register-component",
  moduleId: module.id,
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css",
              "../shared/auth.styles.css"],
})
export class RegisterComponent {
  @Input() errorMessage: string;
  @Input() form: FormGroupState<IRegisterForm>;
  @Input() urlForm: FormGroupState<IUrlForm>;
  @Input() isEmailTaken: boolean;
  @Input() isUrlValid: boolean | undefined;
  @Input() showUrl: string;
  @Input() isExtension: boolean;
  @Input() urlDisplayName: string;
  @Input() hasSubmitStarted: boolean;
  @Input() hasSubmitFinished: boolean;

  @Output() register = new EventEmitter();
  @Output() goToLogin = new EventEmitter<string>();
  @Output() checkEmail = new EventEmitter();
  @Output() checkUrl = new EventEmitter<string>();
  @Output() toggleShowConfirm = new EventEmitter();
  @Output() markAsSubmitted = new EventEmitter();
  @Output() incrementStage = new EventEmitter();
  @Output() switchStage = new EventEmitter<number>();
  @Output() displayUrlInput = new EventEmitter();
  @Output() hideUrlInput = new EventEmitter();
  @Output() setNewsletterSubscribe = new EventEmitter();
  @Output() registrationFinished = new EventEmitter();

  _stageValue: number;
  passwordFocused = false;
  checked: boolean;
  stages = RegisterStages;

  passwordLengthHintText: FormattedString;
  scrollableHeight: number;

  @ViewChild("emailInput") emailInput: AppTextFieldComponent;
  @ViewChild("passwordInput") passwordInput: AppTextFieldComponent;
  @ViewChild("newsletterInput") newsletterInput: ElementRef;
  @ViewChild("registerScrollView") registerScrollView: ElementRef;

  constructor(private actionsSubject: ActionsSubject) {}

  @Input()
  set stageValue(value: RegisterStages) {
    this._stageValue = value;
    setTimeout(() => this.focusNextInput(value));
  }

  get stageValue(): RegisterStages {
    return this._stageValue;
  }

  focusNextInput(value: RegisterStages) {
    switch (value) {
      case this.stages.Email:
        this.emailInput.focusInput();
        return;
      case this.stages.Password:
        this.passwordInput.focusInput();
        return;
      case this.stages.Newsletter:
        this.newsletterInput.nativeElement.focus();
        return;
    }
  }

  submit() {
    switch (this.stageValue) {
      case this.stages.Email:
        this.submitEmail();
        this.passwordLengthHintText = this.buildPasswordLengthHintText();
        return;

      case this.stages.Password:
        if (!this.form.errors._password && !this.form.errors._passwordConfirm) {
          this.incrementStage.emit();
        } else if (this.form.isUnsubmitted) {
          this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
        }
        return;

      case this.stages.Newsletter:
        this.setNewsletterSubscribe.emit();
        this.submitForm();
        return;

      case this.stages.Verified:
        this.registrationFinished.emit();
        return;
    }
  }

  submitEmail() {
    if (this.form.controls.email.isValid) {
      this.checkEmail.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }

  urlSubmit() {
    if (this.urlForm.isValid) {
      this.checkUrl.emit();
    } else {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.urlForm.id));
    }
  }

  switchCurrentStage(value: number) {
    this.switchStage.emit(value);
  }

  displayUrl() {
    this.displayUrlInput.emit();
  }

  hideUrl() {
    this.hideUrlInput.emit();
  }

  submitForm() {
    if (this.form.isValid) {
      this.register.emit();
    } else {
      this.markAsSubmitted.emit();
    }
  }

  toggleDisplay() {
    this.goToLogin.emit();
  }

  goToLoginWith() {
    this.goToLogin.emit(this.form.value.email);
  }

  // https://github.com/NativeScript/nativescript-angular/issues/1119 makes this necessary
  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.form && changes.form.currentValue && changes.form.previousValue) {
      if (changes.form.currentValue.value.password !== changes.form.previousValue.value.password) {
        this.passwordLengthHintText = this.buildPasswordLengthHintText();
      }
      if (changes.form.currentValue.controls.passwordConfirm &&
          changes.form.currentValue.controls.passwordConfirm.isValid &&
          changes.form.previousValue.controls.passwordConfirm.isInvalid) {
        setTimeout(() => this.scrollToBottom(), 100);
      }
    }
  }

  buildPasswordLengthHintText(): FormattedString {
    const formattedString = new FormattedString();

    if (this.form.errors._password && this.form.errors._password.minLength) {
      const makeA = new Span(),
            long = new Span(),
            password = new Span(),
            characterCount = new Span();
      makeA.text = "Make a ";
      long.text = "long";
      long.fontWeight = "bold";
      password.text = " password. ";

      if (this.form.value.password.length < 1) {
        characterCount.text = "We require at least 10\xa0characters.";
      } else if (this.form.value.password.length >= 1) {
        characterCount.text = `Enter at least ${this.form.errors._password.minLength.minLength - this.form.value.password.length} more\xa0character${this.form.errors._password.minLength.minLength - this.form.value.password.length !== 1 ? 's' : ''}.`;
      }

      formattedString.spans.push(makeA, long, password, characterCount);
    } else if (this.form.controls.password.isValid || !this.form.errors._password.minLength) {
      return null;
    }

    return formattedString;
  }

  // Want users to see the text under confirm password
  scrollToBottom() {
    this.scrollableHeight = this.registerScrollView.nativeElement.scrollableHeight;
    this.registerScrollView.nativeElement.scrollToVerticalOffset(this.registerScrollView.nativeElement.scrollableHeight, false)
  }
}

