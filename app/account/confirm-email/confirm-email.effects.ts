import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import * as fromRoot from "~/passit-frontend/app.reducers";
import { withLatestFrom, map, tap } from "rxjs/operators";
import { VerifyEmailSuccess, ConfirmEmailTypes } from "~/passit-frontend/account/confirm-email/confirm-email.actions";

@Injectable()
export class AppConfirmEmailEffects {
  @Effect({ dispatch: false })
  VerifyEmailSuccess$ = this.actions$.pipe(
    ofType<VerifyEmailSuccess>(ConfirmEmailTypes.VERIFY_EMAIL_SUCCESS),
    withLatestFrom(this.store.select(fromRoot.getRouterPath)),
    map(([action, path]) => path),
    tap(path => {
      // The confirm email page automatically redirects while register does not
      if (path === "/confirm-email") {
        this.router.navigate(["/list"], {clearHistory: true});
      }
    })
  );
  constructor(
    private actions$: Actions,
    private router: RouterExtensions,
    private store: Store<fromRoot.IState>
  ) {}
}
