import { Component, EventEmitter, Input, Output, ViewChild, ElementRef } from "@angular/core";
import { ILoginForm } from "../../passit-frontend/account/login/interfaces";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { ActionsSubject } from "@ngrx/store";

@Component({
  selector: "login-component",
  moduleId: module.id,
  templateUrl: "./login.component.html",
  styleUrls: ["../shared/auth.styles.css"],
})
export class LoginComponent  {
  @Input() form: FormGroupState<ILoginForm>;
  @Input() errorMessage: string;
  @Input() hasLoginStarted: boolean;
  @Input() hasLoginFinished: boolean;
  @Output() login = new EventEmitter<ILoginForm>();
  @Output() goToRegister = new EventEmitter();

  @ViewChild("password") password: ElementRef;

  constructor(private actionsSubject: ActionsSubject) {}

  onSubmit() {
    if (this.form.isValid) {
      this.login.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }

  focusPassword() {
    this.password.nativeElement.focus();
  }
}
