import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgrxFormsModule } from "ngrx-forms";

import { ButtonComponent } from "./button/button.component";
import { HeadingComponent } from "./heading/heading.component";
import { AppTextFieldComponent } from "./text-field/text-field.component";
import { NonFieldMessagesComponent } from "./non-field-messages/non-field-messages.component";
import { ProgressIndicatorComponent } from "./progress-indicator/progress-indicator.component";
import { DirectivesModule } from "~/directives";
import { CheckboxComponent } from "./checkbox/checkbox.component";

export const COMPONENTS = [
  AppTextFieldComponent,
  ButtonComponent,
  HeadingComponent,
  NonFieldMessagesComponent,
  ProgressIndicatorComponent,
  CheckboxComponent
];

@NgModule({
  imports: [
	DirectivesModule,
  	NativeScriptCommonModule,
	NgrxFormsModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
	schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule {}
