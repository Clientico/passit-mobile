import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: "app-heading",
  template: `<Label [text]="text" class="app-heading"></Label>`,
  styles: [`.app-heading {
              color: #413741;
              font-size: 24;
              font-weight: bold;
            }`],
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadingComponent {
  @Input() text: string;

  constructor() {}
}
