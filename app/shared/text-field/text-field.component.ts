import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import { AbstractControlState } from "ngrx-forms";

@Component({
  selector: "app-text-field",
  template: `<StackLayout class="input-field m-b-10">
               <Label class="text-label" [text]="label"></Label>
               <TextField
                   #textFieldInput
                   class="input text-field"
                   [class.text-field--valid]="ngrxFormControl.isValid && extraValidations && !overrideValidStyling"
                   [class.text-field--error]="(isFormSubmitted || showErrorBeforeSubmit) && (!ngrxFormControl.isValid || !extraValidations)"
                   [autocapitalizationType]="autocapitalizationType"
                   [autocorrect]="autocorrect"
                   [editable]="editable"
                   [hint]="hint"
                   [keyboardType]="keyboardType"
                   [ngrxFormControlState]="ngrxFormControl"
                   [returnKeyType]="returnKeyType"
                   [secure]="secure"
                   [attr.width]="width ? width : null"
                   (focus)="focus.emit()"
                   (returnPress)="returnPress.emit()"></TextField>
             </StackLayout>`,
  styles: [`.text-label {
              color: #413741;
              font-size: 14;
              letter-spacing: 0.04;
              line-height: 1.333;
            }
            .text-field {
              padding: 10 0;
              border-bottom-width: 2;
              border-bottom-color: #6F989E;
            }
            .text-field::highlighted {
              border-bottom-color: #0092A8;
            }
            .text-field--error::highlighted {
              border-bottom-color: #B92855;
            }
            .text-field--valid::highlighted {
              border-bottom-color: #6CC780;
            }`],
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppTextFieldComponent {
  @Input() autocapitalizationType: string;
  @Input() autocorrect: boolean;
  @Input() editable: boolean = true;
  @Input() isFormSubmitted: boolean;
  @Input() hint: string = "";
  @Input() keyboardType: string;
  @Input() label: string;
  @Input() ngrxFormControl: AbstractControlState<string>;
  @Input() returnKeyType: string;
  @Input() secure: boolean = false;
  @Input() width: number;
  @Input() extraValidations: boolean = true;
  @Input() overrideValidStyling: boolean = false;
  @Input() showErrorBeforeSubmit: boolean = false;
  @Output() returnPress = new EventEmitter();
  @Output() focus = new EventEmitter();
  @ViewChild("textFieldInput") textFieldInput: ElementRef;

  constructor() { }

  focusInput = () => {
    this.textFieldInput.nativeElement.focus();
  }
}
