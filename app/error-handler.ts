import { Sentry } from 'nativescript-sentry';
import { RavenErrorHandler } from "~/passit-frontend/error-handler"

export class SentryErrorHandler extends RavenErrorHandler {
  handleError(err: any) {
    if (this.reportError) {
      Sentry.captureException(err, {});
    } else {
      console.warn("Did not send error report because user did not opt in.");
    }
    console.error(err);
    
  }
}