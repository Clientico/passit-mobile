import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { Store } from "@ngrx/store";

import { AppDataService } from "./passit-frontend/shared/app-data/app-data.service";
import { GetConfAction } from "~/passit-frontend/get-conf/conf.actions";
import { Sentry } from 'nativescript-sentry';
import { getRavenDsn } from "~/passit-frontend/app.reducers";

registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);

@Component({
  selector: "ns-app",
  templateUrl: "app.component.html",
})
export class AppComponent implements OnInit {
  constructor(private appDataService: AppDataService, private store: Store<any>) {
    this.appDataService.rehydrate();
  }

  ngOnInit() {
    this.store.dispatch(new GetConfAction());
    this.store
      .select(getRavenDsn)
      .filter(dsn => dsn !== null)
      .subscribe((dsn: string) => {
        // Workaround for https://github.com/danielgek/nativescript-sentry/issues/15
        if (dsn.length == 57) {
          dsn = dsn.slice(0, 40) + ':' + dsn.slice(40)
        }
        Sentry.init(dsn, null);
      });
  }
}
