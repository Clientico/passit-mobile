FROM alpine:3.7 as testtest
RUN echo $"This is the Repository name {CICD_GIT_REPO_NAME}\nThis is the URL of the Git repository.{CICD_GIT_URL}\nGit commit ID being executed.: {CICD_GIT_COMMIT}\nGit branch of this event.: {CICD_GIT_BRANCH}\nGit reference specification of this event: {CICD_GIT_REF}\nGit tag name, set on tag event.: {CICD_GIT_TAG}\nEvent that triggered the build (push, pull_request or tag).:{CICD_EVENT}\nRancher ID for the pipeline.: {CICD_PIPELINE_ID}\nBuild number of the pipeline.:{CICD_EXECUTION_SEQUENCE}\nCombination of PipelineID and Execution Sequence: {CICD_EXECUTION_ID}\:Address for the Docker registry for the previous publish image step, available in the Kubernetes manifest file of a Deploy YAML step. {CICD_REGISTRY}\nName of the image built from the previous publish image step, available in the Kubernetes manifest file of a Deploy YAML step. It does not contain the image tag.: {CICD_IMAGE}   "





FROM runmymind/docker-android-sdk:ubuntu-standalone as buildApk

ARG NODE_VERSION=8.11.1
ENV NODE_VERSION=8.11.1
#RUN ls
WORKDIR /
COPY *.* /
RUN ls && \
pwd && \
git submodule update --init --recursive && \
  wget -q http://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
 tar -xzf node-v${NODE_VERSION}-linux-x64.tar.gz && \
mv node-v${NODE_VERSION}-linux-x64 /opt/node && \
rm node-v${NODE_VERSION}-linux-x64.tar.gz && \
PATH=${PATH}:/opt/node/bin && \
npm install -g nativescript --unsafe-perm && \
npm install nativescript --unsafe-perm && \
$ANDROID_HOME/tools/bin/sdkmanager "tools" "platform-tools" "platforms;android-26" "build-tools;26.0.2" "extras;android;m2repository" "extras;google;m2repository" > /dev/null && \
npm i && \
echo "build version code CI_PIPELINE_IID" && \
keytool -genkey -v -keystore my-release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias my-alias && \

tns build android --release --bundle --env.snapshot --env.aot --key-store-path my-release-key.jks --key-store-password changeit --key-store-alias-password changeit --key-store-alias my-alias

FROM nginx:1.15
COPY --from=buildApk /app/dist/out/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
#COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf

