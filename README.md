To accomodate learning rancher I created rancher ci process

The basic one echos out certain things:



07:59:39 [pipeline_p-pppph-17] Running shell script
07:59:42 + echo /home/jenkins/workspace/pipeline_p-pppph-17
07:59:42 /home/jenkins/workspace/pipeline_p-pppph-17
07:59:42 + ls
07:59:42 Dockerfile
07:59:42 LICENSE
07:59:42 README.md
07:59:42 SECURITY.md
07:59:42 android26.d.ts
07:59:42 app
07:59:42 bin
07:59:42 declarations.d.ts
07:59:42 docker-compose.yml
07:59:42 fix_sodium.sh
07:59:42 karma.conf.js
07:59:42 package-lock.json
07:59:42 package.json
07:59:42 passit-mobile.sublime-project
07:59:42 references.d.ts
07:59:42 submodules
07:59:42 tsconfig.esm.json
07:59:42 tsconfig.json
07:59:42 webpack.config.js
07:59:42 + echo This is the Repository name passit-mobile
07:59:42 This is the Repository name passit-mobile
07:59:42 + echo This is the URL of the Git repository.https://gitlab.com/Clientico/passit-mobile.git
07:59:42 This is the URL of the Git repository.https://gitlab.com/Clientico/passit-mobile.git
07:59:42 + echo Git commit ID being executed.: 63f6f97
07:59:42 Git commit ID being executed.: 63f6f97
07:59:42 + echo Git branch of this event.: master
07:59:42 Git branch of this event.: master
07:59:42 + echo Git reference specification of this event: refs/heads/master
07:59:42 Git reference specification of this event: refs/heads/master
07:59:42 + echo Git tag name, set on tag event.: 
07:59:42 Git tag name, set on tag event.: 
07:59:42 + echo Event that triggered the build (push, pull_request or tag).:push
07:59:42 Event that triggered the build (push, pull_request or tag).:push
07:59:42 + echo Rancher ID for the pipeline.: p-pppph
07:59:42 Rancher ID for the pipeline.: p-pppph
07:59:42 + echo Build number of the pipeline.:17
07:59:42 Build number of the pipeline.:17
07:59:42 + echo Combination of PipelineID and Execution Sequence: p-pppph-17
07:59:42 Combination of PipelineID and Execution Sequence: p-pppph-17
07:59:42 + echo Address for the Docker registry for the previous publish image step, available in the Kubernetes manifest file of a Deploy YAML step.: 
07:59:42 Name of the image built from the previous publish image step, available in the Kubernetes manifest file of a Deploy YAML step. It does not contain the image tag.: 
07:59:42 Address for the Docker registry for the previous publish image step, available in the Kubernetes manifest file of a Deploy YAML step.: 
07:59:42 Name of the image built from the previous publish image step, available in the Kubernetes manifest file of a Deploy YAML step. It does not contain the image tag.: 

So I would like to generate my openssl here as well before we start
gcc:latest is the base image from which I am running commands




# Passit Mobile

Passit Mobile is a Nativescript app for Android (and maybe one day ios). It reuses the angular code from [passit-frontend](https://gitlab.com/passit/passit-frontend/)

Get the app now on [Google Play](https://play.google.com/store/apps/details?id=com.burkesoftware.Passit) or from [Gitlab CI](https://gitlab.com/passit/passit-mobile/pipelines).

We follow the redux principle of presentational vs smart components. Typically the app requires it's own module (so as to selectively import parts of the main app). The module just imports the frontend's reducers and smart components. Sometimes it extends them for mobile specific features. Then it provides the presentational componenents that output native elements instead of html.

## Development

Ensure you have nativescript [installed](https://docs.nativescript.org/angular/start/quick-setup) and a working Android SDK with emulator or device. If using Linux we recommend the [full installation instructions](https://docs.nativescript.org/angular/start/ns-setup-linux). Run `tns doctor` to ensure no errors come up.

If using Linux - edit `~/.bashrc` and add `export ANDROID_EMULATOR_USE_SYSTEM_LIBS=1` or else the emulator won't start.

After cloning the repo:

1. `git submodule init`
2. `git submodule update`
3. `npm i`
4. `tns run android`

## Troubleshooting

The world of Nativescript Mobile development is a wild one where things stop working for no apparent reason. If something isn't working try this:

- Stop tns run and start it again
- Run `./fix_sodium.sh` this script should run after npm i but doesn't always.
- Reinstall node packages `rm -rf node_modules` and `npm i`
- Remove and readd nativescript platform `tns platform remove android; tns platform add android`
- Update your Android SDK and emulator. Try deleting the emulator and remaking it.